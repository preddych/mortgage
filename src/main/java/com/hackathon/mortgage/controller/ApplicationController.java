package com.hackathon.mortgage.controller;

import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.ApplicationService;

@RestController
@CrossOrigin

public class ApplicationController {
	@Autowired 
	ApplicationService applicationService;
	
	Logger logger = LoggerFactory.getLogger(ApplicationController.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/loan", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<Application> createCustomer(@RequestBody Application application) {

		logger.info("---Inside createCustomer()--");
		applicationService.create(application);

		logger.info("---Completed createCustomer()--");
		return new ResponseEntity(application, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/loan", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<List<Application>> getMortgageDetails() {

		logger.info("---Inside getMortgageDetails()--");
		return new ResponseEntity(applicationService.fetchAllLoanDetails(), HttpStatus.OK);
	}

}