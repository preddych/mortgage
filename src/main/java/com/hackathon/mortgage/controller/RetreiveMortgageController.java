package com.hackathon.mortgage.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.mortgage.exception.LoanNotFoundException;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.RetreiveLoanIdService;

@RestController
@CrossOrigin
public class RetreiveMortgageController {

	
	private final Logger logger = LoggerFactory.getLogger(RetreiveMortgageController.class);
	 
	
	@Autowired
	RetreiveLoanIdService retLoanSer;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value="/loan/{id}", method=RequestMethod.GET)
	public ResponseEntity<Application> getLoanDetails(@PathVariable("id") int id,HttpServletRequest  httpReq) throws LoanNotFoundException
	{
		logger.info("---Inside getMortgageDetails()--");
		return new ResponseEntity(retLoanSer.getLoanId(id), HttpStatus.OK);
	}
	
}
