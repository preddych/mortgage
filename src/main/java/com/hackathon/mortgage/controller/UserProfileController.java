package com.hackathon.mortgage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.mortgage.model.UserProfile;
import com.hackathon.mortgage.service.UserProfileService;


/**
 * @author Pranavi 
 * Controller class to handle UserProfile
 */
@RestController
@CrossOrigin
public class UserProfileController {

	private static final Logger logger = LoggerFactory.getLogger(UserProfileController.class);

	@Autowired
	private UserProfileService userProfileService;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/updateProfile", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<UserProfile> updateUserProfile(@RequestBody UserProfile userProfile) {

		logger.info("---Inside updateProfile()--");
		userProfileService.updateUserProfile(userProfile);

		logger.info("---Completed updateProfile()--");
		return new ResponseEntity(userProfile, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/broker", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ResponseEntity<UserProfile> getUserProfile(@RequestBody UserProfile profile) {

		logger.info("---Inside getUserProfile()--");
		UserProfile userProfile= userProfileService.getUserProfile(profile.getProfileId());

		logger.info("---Completed getUserProfile()--");
		return new ResponseEntity(userProfile, HttpStatus.OK);
	}
}
