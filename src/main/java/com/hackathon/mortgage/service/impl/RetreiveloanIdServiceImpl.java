package com.hackathon.mortgage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.mortgage.dao.RetreiveLoanDao;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.RetreiveLoanIdService;

@Service
public class RetreiveloanIdServiceImpl implements RetreiveLoanIdService{

	
	@Autowired
	RetreiveLoanDao retreiveLoanDao;
	
	
	public Application getLoanId(int aID)
	{
		return retreiveLoanDao.retreiveLoanId(aID);
		
		
	}
}
