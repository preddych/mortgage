package com.hackathon.mortgage.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hackathon.mortgage.constants.SqlConstants;
import com.hackathon.mortgage.dao.RetreiveLoanDao;
import com.hackathon.mortgage.model.Application;


@Repository
public class RetreiveLoanDaoImpl implements RetreiveLoanDao {

	@Autowired
	JdbcTemplate jdbcTemlate;
	
	public Application retreiveLoanId(int loanId)
	{
		return jdbcTemlate.queryForObject(SqlConstants.LOANSQL, new Object[]{loanId},
				new BeanPropertyRowMapper<>(Application.class));

	}

}
