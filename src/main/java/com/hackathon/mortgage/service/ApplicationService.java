package com.hackathon.mortgage.service;

import java.util.List;

import com.hackathon.mortgage.model.Application;

public interface ApplicationService {
 Application create(Application application);
 
 List<Application> fetchAllLoanDetails();
 
}
