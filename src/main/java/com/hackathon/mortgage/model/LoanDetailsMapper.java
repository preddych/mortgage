package com.hackathon.mortgage.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

public class LoanDetailsMapper implements RowMapper<Application>{
	
	Logger logger = LoggerFactory.getLogger(LoanDetailsMapper.class);
	
	@Override
	public Application mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
		
		logger.info("---Inside LoanDetailsMapper.mapRow()---");
		Application mortgageDetails = new Application();
		mortgageDetails.setDealNo(resultSet.getInt("dealNo"));
		mortgageDetails.setFname(resultSet.getString("fname"));
		mortgageDetails.setLname(resultSet.getString("lname"));
		mortgageDetails.setAmount(resultSet.getDouble("amount"));
		mortgageDetails.setLoanType(resultSet.getString("loanType"));
		mortgageDetails.setPropertyAddress(resultSet.getString("propertyAddress"));
		mortgageDetails.setMonthlyIncome(resultSet.getDouble("monthlyIncome"));
		mortgageDetails.setMonthlyExpenses(resultSet.getDouble("monthlyExpenses"));
		mortgageDetails.setLoanTerm(resultSet.getInt("loanTerm"));
		mortgageDetails.setRepaymentFrequency(resultSet.getString("repaymentFrequency"));
		mortgageDetails.setStatus(resultSet.getString("status"));
		mortgageDetails.setDob(resultSet.getString("dob"));
		logger.info("---Submitted Date-----"+resultSet.getString("submittedDate"));
		mortgageDetails.setSubmittedDate(resultSet.getString("submittedDate"));
		
		logger.info("---Done LoanDetailsMapper.mapRow()---");
		return mortgageDetails;
	}

	
}
