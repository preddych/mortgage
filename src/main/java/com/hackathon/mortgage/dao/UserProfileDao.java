/**
 * 
 */
package com.hackathon.mortgage.dao;

import com.hackathon.mortgage.model.UserProfile;

/**
 * @author Hackathon
 *
 */
public interface UserProfileDao {

	public int updateUserProfile(UserProfile userProfile );
	public UserProfile getUserProfile(String profileId);

}
