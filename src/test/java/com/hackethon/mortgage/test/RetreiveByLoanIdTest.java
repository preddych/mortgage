package com.hackethon.mortgage.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.hackathon.mortgage.dao.RetreiveLoanDao;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.impl.RetreiveloanIdServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class RetreiveByLoanIdTest {

	@InjectMocks
	private RetreiveloanIdServiceImpl retLoanService;

	@Mock
	private RetreiveLoanDao retreiveLoanDao;
	Application testApp;

	@Before
	public void init() {
		testApp = new Application();
		testApp.setAmount(1000);
		testApp.setFname("Vivek");
		testApp.setDealNo(1);
	}

	@Test
	public void testRetreiveLoanById() {
		when(retreiveLoanDao.retreiveLoanId(1)).thenReturn(testApp);
		assertEquals(testApp.getDealNo(), retLoanService.getLoanId(1).getDealNo());
		verify(retreiveLoanDao).retreiveLoanId(testApp.getDealNo());
	}

	@Test
	public void negativeTestRetreiveLoanById() {
		when(retreiveLoanDao.retreiveLoanId(100)).thenReturn(testApp);
		assertNull("Load Id doesn't Exists", retLoanService.getLoanId(testApp.getDealNo()));
	}
}