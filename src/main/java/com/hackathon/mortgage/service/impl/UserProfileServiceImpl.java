package com.hackathon.mortgage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.mortgage.dao.UserProfileDao;
import com.hackathon.mortgage.model.UserProfile;
import com.hackathon.mortgage.service.UserProfileService;


@Service
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	UserProfileDao userProfileDao;

	@Override
	public UserProfile updateUserProfile(UserProfile userProfile) {
		userProfileDao.updateUserProfile(userProfile);
		return userProfile; 
	}
	
	@Override
	public UserProfile getUserProfile(String profileId) {
		return userProfileDao.getUserProfile(profileId);
		 
	}

}
