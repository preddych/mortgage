package com.hackathon.mortgage.service;

import com.hackathon.mortgage.model.UserProfile;

public interface UserProfileService {
	UserProfile updateUserProfile(UserProfile userProfile);
	UserProfile getUserProfile(String profileId);

}
