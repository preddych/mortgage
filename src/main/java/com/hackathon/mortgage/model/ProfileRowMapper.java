package com.hackathon.mortgage.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.springframework.jdbc.core.RowMapper;

public class ProfileRowMapper implements RowMapper<UserProfile>
{
	@Override
	public UserProfile mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserProfile user= new UserProfile();
		user.setProfileId(rs.getString("profileid"));
		user.setEmailId(rs.getString("emailid"));
		user.setPhoneNo(rs.getLong("phoneno"));
		user.setAddress(rs.getString("address"));
		user.setDob(new SimpleDateFormat("yyyy-MM-dd").format(rs.getDate("dob")));
		user.setPwd(rs.getString("pwd"));
		return user;
	}

}
