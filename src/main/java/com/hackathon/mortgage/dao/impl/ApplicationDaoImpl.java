package com.hackathon.mortgage.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hackathon.mortgage.dao.ApplicationDao;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.model.LoanDetailsMapper;

@Repository
public class ApplicationDaoImpl implements ApplicationDao {

	@Autowired
	JdbcTemplate jdbcTemlate;
	
	@Override
	public int createApplication(Application application) {
		jdbcTemlate.update("insert into mortgage_details(fname,lname,amount,dob,loanTerm,loanType,monthlyExpenses,monthlyIncome,repaymentFrequency,propertyAddress,status,submittedDate) values(?,?,?,?,?,?,?,?,?,?,'Pending',sysdate())",
				application.getFname(),
				application.getLname(),
				application.getAmount(),
				application.getDob(),
				application.getLoanTerm(),
				application.getLoanType(),
				application.getMonthlyExpenses(),
				application.getMonthlyIncome(),
				application.getRepaymentFrequency(),
				application.getPropertyAddress());
		
		return 0;
	}


	@Override
	public List<Application> fetchAllLoanDetails() {
		String fetchAllLoanDetailsQuery = "SELECT * FROM mortgage_details;";
		
		return jdbcTemlate.query(fetchAllLoanDetailsQuery, new LoanDetailsMapper());	
	}
	
}
