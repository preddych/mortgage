package com.hackethon.mortgage.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.hackathon.mortgage.dao.UserProfileDao;
import com.hackathon.mortgage.model.UserProfile;
import com.hackathon.mortgage.service.impl.UserProfileServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserProfileServiceTest {

	@Mock
	UserProfileDao userProfileDao;


	@InjectMocks
	UserProfileServiceImpl userProfileServiceImpl;

	UserProfile user1= null;
	UserProfile user2= null;
	UserProfile user3= null;

	List<UserProfile> userProfileList = new ArrayList<>();

	@Before
	public void init() {
		user1 = new UserProfile();
		user1.setProfileId("test1@xx.com");
		user1.setPhoneNo(998444445);
		user1.setDob("1991-06-25");
		user1.setAddress("Bommadandra, Bangalore");
		user1.setPwd("xxxx");
		
		user2 = new UserProfile();
		user2.setProfileId("test2@xx.com");
		user2.setPhoneNo(988444455);
		user2.setDob("1992-04-05");
		user2.setAddress("Jigani, Bangalore");
		user2.setPwd("xxxx");
		
		userProfileList.add(user1);
		userProfileList.add(user2);
	}
	
	@Test
	public void testUpdateProfile() {
		when(userProfileDao.updateUserProfile(user1)).thenReturn(0);
		assertEquals(1, 998444445, userProfileServiceImpl.updateUserProfile(user1).getPhoneNo());
		verify(userProfileDao).updateUserProfile(user1);
	}
	@Test

    public void testGetProfile() {
        when(userProfileDao.getUserProfile(user2.getProfileId())).thenReturn(user2);
        assertEquals(1,988444455, userProfileServiceImpl.getUserProfile(user2.getProfileId()).getPhoneNo());
        verify(userProfileDao).getUserProfile(user2.getProfileId());
    }

    @Test
    public void negativeTestGetProfile() {
        when(userProfileDao.getUserProfile("test3@yyy.com")).thenReturn(user2);
        assertNull("Invalid User", userProfileServiceImpl.getUserProfile(user2.getProfileId()));
    }

}
