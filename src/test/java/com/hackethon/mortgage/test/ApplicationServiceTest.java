package com.hackethon.mortgage.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.hackathon.mortgage.dao.ApplicationDao;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.impl.ApplicationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceTest {

	@Mock
	ApplicationDao applicationDao;


	@InjectMocks
	ApplicationServiceImpl appServiceImpl;

	Application app1= null;
	Application app2= null;
	Application app3= null;
	Application app4= null;

	List<Application> appList = new ArrayList<>();

	@Before
	public void init() {
		app1 = new Application();
		app1.setDealNo(1);
		app1.setAmount(100000);
		app1.setDob("1987-12-12");
		app1.setFname("FName1");
		app1.setLname("LName1");
		app1.setLoanTerm(180);
		app1.setLoanType("F");
		app1.setMonthlyExpenses(10000);
		app1.setMonthlyIncome(80000);
		app1.setPropertyAddress("Jigani, Bangalore");
		app1.setRepaymentFrequency("Yearly");
		app1.setStatus("Pending");
		app1.setSubmittedDate("2017-12-12");
		
		app2 = new Application();
		app2.setDealNo(2);
		app2.setAmount(200000);
		app2.setDob("1985-11-10");
		app2.setFname("FName2");
		app2.setLname("LName2");
		app2.setLoanTerm(220);
		app2.setLoanType("V");
		app2.setMonthlyExpenses(20000);
		app2.setMonthlyIncome(85000);
		app2.setPropertyAddress("Bommasandra, Bangalore");
		app2.setRepaymentFrequency("Yearly");
		app2.setStatus("Pending");
		app2.setSubmittedDate("2017-10-11");
		

		app3 = new Application();
		app3.setDealNo(3);
		app3.setAmount(300000);
		app3.setDob("1988-09-10");
		app3.setFname("FName3");
		app3.setLname("LName3");
		app3.setLoanTerm(160);
		app3.setLoanType("V");
		app3.setMonthlyExpenses(25000);
		app3.setMonthlyIncome(95000);
		app3.setPropertyAddress("Electronic City, Bangalore");
		app3.setRepaymentFrequency("Yearly");
		app3.setStatus("Pending");
		app3.setSubmittedDate("2016-04-11");

		appList.add(app1);
		appList.add(app2);
		appList.add(app3);
	}
	
	@Test
	public void testCreate() {
		when(applicationDao.createApplication(app1)).thenReturn(0);
		assertEquals(1, 1, appServiceImpl.create(app1).getDealNo());
		verify(applicationDao).createApplication(app1);

	}

	@Test
	public void testFetchAllLoanDetails() {
		when(applicationDao.fetchAllLoanDetails()).thenReturn(appList);
		assertEquals(3, appServiceImpl.fetchAllLoanDetails().size());
		verify(applicationDao).fetchAllLoanDetails();
	}
	
	 @Test
     public void negativeTestFetchAllLoanDetails() {
         when(applicationDao.fetchAllLoanDetails()).thenReturn(appList);
         assertEquals(3, appServiceImpl.fetchAllLoanDetails().size());
         verify(applicationDao).fetchAllLoanDetails();
     }
     @Test
     public void negativeTestCreate() {
         when(applicationDao.createApplication(app4)).thenReturn(0);
         assertNull("Error while applying loan",  appServiceImpl.create(app4));
     }

}
