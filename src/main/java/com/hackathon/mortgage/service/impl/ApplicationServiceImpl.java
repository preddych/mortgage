package com.hackathon.mortgage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.mortgage.dao.ApplicationDao;
import com.hackathon.mortgage.model.Application;
import com.hackathon.mortgage.service.ApplicationService;

@Service
public class ApplicationServiceImpl implements ApplicationService {

	@Autowired
	ApplicationDao applicationDao;
	
	@Override
	public Application create(Application application) {
		applicationDao.createApplication(application);
		return application;
	}
	

	@Override
	public List<Application> fetchAllLoanDetails() {
		return applicationDao.fetchAllLoanDetails();
	}

}


