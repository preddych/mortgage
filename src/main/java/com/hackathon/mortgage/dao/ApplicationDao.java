package com.hackathon.mortgage.dao;

import java.util.List;

import com.hackathon.mortgage.model.Application;

public interface ApplicationDao {
	int createApplication(Application application);
	
	List<Application> fetchAllLoanDetails();

	
}
