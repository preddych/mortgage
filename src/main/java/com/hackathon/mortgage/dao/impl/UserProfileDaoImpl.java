package com.hackathon.mortgage.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hackathon.mortgage.dao.UserProfileDao;
import com.hackathon.mortgage.model.ProfileRowMapper;
import com.hackathon.mortgage.model.UserProfile;

@Repository
public class UserProfileDaoImpl implements UserProfileDao {
	
	private static final Logger logger = LoggerFactory.getLogger(UserProfileDaoImpl.class);
	
	//private static final String UPDATE_USER_PROFILE_QUERY = "INSERT INTO profile_data (profileId, emailId, phoneNo, address, dob, pwd) VALUES (?, ?, ?, ?, ?,? )";
	private static final String UPDATE_USER_PROFILE_QUERY = "update profile_data set emailId=? , phoneNo=?, address=?, dob=?, pwd=? where profileId=?";
	private static final String GET_USER_PROFILE_QUERY = "SELECT * FROM profile_data where profileId = ? ";

	@Autowired
	JdbcTemplate jdbcTemplate;

	public int updateUserProfile(UserProfile userProfile) {
		logger.info("updateUserProfile in UserProfileDaoImpl " );
		return jdbcTemplate.update(UPDATE_USER_PROFILE_QUERY, userProfile.getEmailId(),userProfile.getPhoneNo(),
				userProfile.getAddress(),userProfile.getDob(), userProfile.getPwd(),userProfile.getProfileId());
	}
	
	@Override
	public UserProfile getUserProfile(String profileId) {
		logger.info("getUserProfile in UserProfileDaoImpl ");
		return jdbcTemplate.queryForObject(GET_USER_PROFILE_QUERY, new Object[] { profileId }, new ProfileRowMapper());	
	}
	
}
